package com.example.PostTry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostTryApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostTryApplication.class, args);
	}

}
