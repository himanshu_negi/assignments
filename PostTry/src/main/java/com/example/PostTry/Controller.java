package com.example.PostTry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
@Autowired
private UserRepository userRepository;
	
	
	@GetMapping("/getdata")
	public List<User> getdata()
	{List<User> list =userRepository.findAll();
		return list;
	}
}
