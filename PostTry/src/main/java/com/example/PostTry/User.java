package com.example.PostTry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="usertest")
@Data
public class User {
	@Id
	private int id;
	@Column(name="name")
	private String name;
	@Column(name="designation")
	private String designation;

}
