package com.saaim.task1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.saaim.task1.model.User;
import com.saaim.task1.repository.UserRepository;

@RestController
@RequestMapping("/api")
public class UserController {
	
	
	private int count=0;
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/user")
	public List<User> getAllUsers(){
		List<User> u = this.userRepository.findAll();
		return u;
	}
	
	@PostMapping("/user")
	public int addUsers(@RequestBody List<User> user) {
		count=user.size();
		int uploadID = (int)(Math.random()*(100-1+1)+1);
		for(User u : user) {
			u.setUploadID(uploadID);
			this.userRepository.save(u);
			try{Thread.sleep(2000);}catch(Exception e) {};
		}
		
		return uploadID;
		//return this.userRepository.save(user).getId();
	}
	
	
	@GetMapping("/user/{uploadID}")
	public String check(@PathVariable(value="uploadID") int uploadID) {
		
		List<User> result = userRepository.findAll();
		int tempCount=0;
		for(User u : result) {
			if(u.getUploadID()==uploadID)
				tempCount++;
		}
		if(tempCount == count)
			return "All added";
		else
			return tempCount + " added";
		
		
	}
	
//	@GetMapping("/check")
//	public User checkUploaded() {
//		String uri = "http://localhost:8080//api2";
//		RestTemplate restTemplate = new RestTemplate();
//		User result = restTemplate.getForObject(uri, User.class);
//		return result;
//		
//	}
	
//	@RequestMapping("/check")
//	public User check() {
//		User user = new User(28,"temp","advv",4);
//		if(userRepository.findAll().size()==count)
//			System.out.println( "############# All added");
//		else
//			System.out.println( "##############"+userRepository.findAll().size()+" added\nRetry in 2 seconds...");
//		return user;
//	
//	}
	
	
}
