package com.spring.JwtSpring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.spring.JwtSpring.helper.JwtUtil;
import com.spring.JwtSpring.model.JwtRequest;
import com.spring.JwtSpring.model.JwtResponse;
import com.spring.JwtSpring.service.CustomUserDetailsService;

@RestController
public class JwtController {

	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private JwtUtil jwtUtil;

	@RequestMapping(value = "/token", method = RequestMethod.POST)
	public ResponseEntity<?> generateToken(@RequestBody JwtRequest jwtRequest) throws Exception {
		System.out.println(jwtRequest);

		try {
			//this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(jwtRequest.getUsername(), jwtRequest.getPassword()));
		} catch (UsernameNotFoundException e) {
			System.out.println(e);
			 e.printStackTrace();
			throw new Exception("bad credintials1   1");
		}
catch(BadCredentialsException e)
		{
	System.out.println(e);
	 e.printStackTrace();
	throw new Exception("bad credintials 2 2 2 ");
		}
		UserDetails userDetails = this.customUserDetailsService.loadUserByUsername(jwtRequest.getUsername());
		String token = this.jwtUtil.generateToken(userDetails);
		System.out.println("jet token " + token);

		return ResponseEntity.ok(new JwtResponse(token));

	}

	@RequestMapping(value = "home", method = RequestMethod.GET)
	public String print() {
		return "u r authorised user";
	}

	@RequestMapping(value="welcome",method=RequestMethod.GET)
	public String printhome()
	{
		
    return "  this is for testing purpose no token required ";
		
	}

	@RequestMapping(value="admin",method=RequestMethod.GET)
	public String admin()
	{
		
		
		return "  this is admin";
		
	}
}
