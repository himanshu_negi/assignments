package com.spring.JwtSpring.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.stereotype.Service;


@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		if (username.equals("himanshu")) {
			List<GrantedAuthority>auth= new ArrayList<>();
			auth.add( new SimpleGrantedAuthority("ADMIN"));

return new User("himanshu", "abc1", auth);
		} 
		
		else if (username.equals("anshu")) {

return new User("anshu", "abc1", new ArrayList<>());
		} 
		
		
		
		
		else {
			throw new UsernameNotFoundException("user not found");
		}
	


	}

	
	
	
	
	
}
