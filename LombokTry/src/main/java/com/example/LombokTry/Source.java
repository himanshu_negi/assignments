package com.example.LombokTry;

import lombok.Data;

@Data
public class Source {
	private int id;
	
	private String name;
	private String location;
}
