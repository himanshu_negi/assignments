package com.example.FilterTry;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;



@Component

public class FilterChecks {
	Map<String, Integer> apimap;
	Map<String, Integer> usermincount;
	Map<String, LocalTime> timemap;
	Map<String, List<LocalTime>> usertimemap;
	

	public FilterChecks() {
		apimap = new HashMap<String, Integer>();
		timemap = new HashMap<String, LocalTime>();
		usermincount = new HashMap<String, Integer>();
		usertimemap = new HashMap<String, List<LocalTime>>();
		
	}

	public int ApitotalCount(String apiUrl) {

		if (apimap.containsKey(apiUrl)) {
			apimap.put(apiUrl, apimap.get(apiUrl) + 1);
			return apimap.get(apiUrl);
		} else

		{
			apimap.put(apiUrl, 1);
		}

		return apimap.get(apiUrl);
	}

	public LocalTime Apitimecheck(String apiUrl) {
		LocalTime currentime = LocalTime.now();

		if (timemap.containsKey(apiUrl)) {
			if (currentime.compareTo(timemap.get(apiUrl)) > 0) {
				timemap.put(apiUrl, currentime.plusMinutes(1));
				apimap.remove(apiUrl);

			}
		} else

		{
			timemap.put(apiUrl, currentime.plusMinutes(1));
		}

		return timemap.get(apiUrl);
	}

	public int UserHitCount(String user) {

		if (usermincount.containsKey(user)) {
			usermincount.put(user, usermincount.get(user) + 1);
			return usermincount.get(user);
		} else

		{
			usermincount.put(user, 1);
		}

		return usermincount.get(user);
	}

	public List<LocalTime> usertimecheck(String user) {

		LocalTime currentime = LocalTime.now();
		List<LocalTime> list = new ArrayList<>();

		if (usertimemap.containsKey(user)) {
			List<LocalTime> check = usertimemap.get(user);

			if (currentime.compareTo(check.get(0)) > 0) {

				check.set(0, currentime.plusMinutes(1));

				usertimemap.put(user, check);

				usermincount.remove(user);
				

			}
			if (currentime.compareTo(check.get(1)) > 0) {

				check.set(1, currentime.plusHours(1));
				usertimemap.put(user, check);
	         	MyFilter.count=0;
			}
		} else

		{
			list.add(currentime.plusMinutes(1));
			list.add(currentime.plusHours(1));
			usertimemap.put(user, list);
		}

		return usertimemap.get(user);
	}

	

}
