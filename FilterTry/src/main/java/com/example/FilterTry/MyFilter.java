package com.example.FilterTry;

import java.io.IOException;
import java.time.LocalTime;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MyFilter implements Filter {
	@Autowired
	private FilterChecks filterChecks;
	static int count;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest http = (HttpServletRequest) request;
		String apiUrl = http.getRequestURI();
		log.info("api{}",apiUrl);
		String user = http.getLocalAddr();

		int totalApicount = filterChecks.ApitotalCount(apiUrl);
		LocalTime getAllowedTime = filterChecks.Apitimecheck(apiUrl);

		int userHitCountmin = filterChecks.UserHitCount(user);
		List<LocalTime> getuserallowedtime = filterChecks.usertimecheck(user);
		

		log.info("Api hit at {}", LocalTime.now());
		log.info("allowed  till time is {}", getuserallowedtime.get(0));
		
		if (  (userHitCountmin <= 10) && (LocalTime.now().compareTo(getuserallowedtime.get(0)) < 0)&& (totalApicount <= 150) && (LocalTime.now().compareTo(getAllowedTime) < 0)) {
		
			
			count++;
			if((count <= 30)&&(LocalTime.now().compareTo(getuserallowedtime.get(1)) < 0))
			{log.info("no of time {} api is hit by user {} is {} times in hour and {} times in min }", apiUrl, user,count,userHitCountmin);
			
			chain.doFilter(request, response);
			}		} else {
			log.warn(" u have exceeded  api hit limit");
		}
	}

	@Override
	public void destroy() {
	}

}
