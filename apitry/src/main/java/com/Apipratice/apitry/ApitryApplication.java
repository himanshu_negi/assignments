package com.Apipratice.apitry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApitryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApitryApplication.class, args);
	}

}
