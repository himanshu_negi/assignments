package com.Apipratice.apitry.Resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.Apipratice.apitry.ApiEntity;

@Service

public class ApiResource implements ApiInterface {

	List<ApiEntity> list = new ArrayList<>();

	public ApiResource() {
		 list.add( new ApiEntity(1,"himanshu","mca"));
		 list.add( new ApiEntity(2,"himanshi","bca"));
		 list.add( new ApiEntity(3,"anshu","msc"));
	}

	@Override
	public List<ApiEntity> getData() {
		
		return list;
	}

	
	
	
}
