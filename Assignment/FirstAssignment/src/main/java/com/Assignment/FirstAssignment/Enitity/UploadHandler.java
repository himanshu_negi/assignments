package com.Assignment.FirstAssignment.Enitity;

import java.util.List;

public class UploadHandler {
Integer uploadid;
List< UserEntity> users;
boolean status;
public UploadHandler(Integer uploadid, List<UserEntity> users, boolean status) {
	super();
	this.uploadid = uploadid;
	this.users = users;
	this.status = status;
}
public Integer getUploadid() {
	return uploadid;
}
public void setUploadid(Integer uploadid) {
	this.uploadid = uploadid;
}
public List<UserEntity> getUsers() {
	return users;
}
public void setUsers(List<UserEntity> users) {
	this.users = users;
}
public boolean getStatus() {
	return status;
}
public void setStatus(boolean status) {
	this.status = status;
}
@Override
public String toString() {
	return "UpoadHandler [uploadid=" + uploadid + ", users=" + users + ", status=" + status + "]";
}






}
