package com.Assignment.FirstAssignment;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Assignment.FirstAssignment.Enitity.UploadHandler;
import com.Assignment.FirstAssignment.Enitity.UserEntity;

import com.Assignment.FirstAssignment.Repo.UserRepository;

@RestController
@RequestMapping("/db")
public class UserController {
	public int count = 0;
	List<UploadHandler> uploadhandler = new ArrayList<>();
	private int userid =0;
	@Autowired
	UserRepository userrepository;

	boolean result = true;

	@PostMapping("/data")

	public int createEmployee(@RequestBody List<UserEntity> user) {
		int tempid = 0;
		count = user.size();

		// Set<Integer> set = new HashSet<Integer>();
		while (true) {
			tempid = (int) (Math.random() * (100 - 1 + 1) + 1);
			// if(!set.contains(tempid))
			if (userrepository.findByUserid(tempid).size() == 0) {
				// userid=tempid;
				// set.add(tempid);

				break;
			}
		}
		final int uploadID = tempid;
		uploadhandler.add(new UploadHandler(uploadID, user, false));

		ExecutorService parent = Executors.newFixedThreadPool(1);

		for (UploadHandler uh : uploadhandler) {
			if (uh.getStatus() == false) {

				parent.submit(() -> {

					ExecutorService executorservice = Executors.newFixedThreadPool(3);

					for (UserEntity u : user) {
						executorservice.submit(() -> {
							u.setUserid(uploadID);
							this.userrepository.save(u);
							try {
								Thread.sleep(7000);
							} catch (Exception e) {
							}
							;

						});

					}

					executorservice.shutdown();

				});
				uh.setStatus(true);

			}

		}
		return uploadID;

	}

	@GetMapping("/data/{userid}")

	public List<String> getEmployeebyId(@PathVariable int userid) {
		List<UserEntity> allUserlist = userrepository.findByUserid(userid);

		List<String> newobj = new ArrayList<>();
		for (UserEntity e : allUserlist) {
			if (userid == e.getUserid())

				newobj.add(e.toString());
		}

		for (UploadHandler h : uploadhandler) {
			if (h.getUploadid() == userid) {
				count = h.getUsers().size();
				break;
			}

		}

		if (count != newobj.size()) {
			newobj.add("retry in 2 seconds");
		} else {
			newobj.add("alladded");
		}

		return newobj;
	}
}

//	@GetMapping("/data/{id}")
//	
//	public List<String> getEmployeebyId(@PathVariable int  id)
//{   int check=0;
//	List<UserEntity> allUserlist = userrepository.findAll();
//	
//	List<String> newobj =new ArrayList<>();
//	for(UserEntity e:allUserlist)
//	{   if(id==e.getUserid())
//		
//		newobj.add(e.toString());
//	}
//	while(check<1) 
//{
//		if(count!=newobj.size())
//		{
//		//System.out.println("retry in 2 seconds");
//		newobj.add("retry in 2 seconds");
//		}
//		else
//		{newobj.add("alladded");
//			}
//		check++;}
//		return newobj;
//}

//	@GetMapping("/data")
//	
//	public List<UserEntity> getEmployeebyId()
//{   int check=0;
//	List<UserEntity> allUserlist = userrepository.findAll();
//	
//	List<UserEntity> newobj =new ArrayList<>();
//	
//
//	
//	
//	for(UserEntity e:allUserlist)
//	{   if(userid==e.getUserid())
//		newobj.add(e);
//	}
//	while(check<1) 
//{
//		if(count!=newobj.size())
//		{
//			System.out.println("retry in 2 seconds");		
//		}
//		check++;}
//		return newobj;

//}

//	@Autowired
//	RestTemplate restTemplate;
//	@GetMapping("/getdata")
//	public List<UserEntity> getAllData(){
//	String url = "http://localhost:8080/db/data";
//	List<UserEntity> data = null;
//	try
//	{ResponseEntity<List<UserEntity>> response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<UserEntity>>() {});
//	 data= response.getBody();
//	}
//	catch(HttpClientErrorException e)
//	{}
//	return data;
//	}
//	@GetMapping("/data/{id}")
//	public UserEntity getEmployeebyId(@PathVariable(value = "id") Integer userid)     
//	{
//	UserEntity userEntity = userrepository.findById(userid).get();	
//		return userEntity;
//	}
//	@Autowired
//	RestTemplate restTemplate;
//	@GetMapping("/getdata")	
//	public List<UserEntity> getAllData(){
//	//@PathVariable String userid ){
//		String rtrn ="rety in 2 seconds";
//	String url = "http://localhost:8080/db/data";
//	List<UserEntity> data = null;
//	List<UserEntity> newobj =new ArrayList<>();
//	List<String> str= new ArrayList<>();
//try
//	{ResponseEntity<List<UserEntity>> response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<UserEntity>>() {});
//	 data= response.getBody();
//int uploadid=Integer.parseInt(userid);
//	//System.out.println(uploadid);
//	for(UserEntity e:data)
//	{if(userid==e.getUserid())
//		newobj.add(e);
//	}	
//	}
//catch(HttpClientErrorException e)
//	{}
//
//return newobj;
//
//	
//	}
//return newobj;
