package com.Assignment.FirstAssignment.Enitity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="userdata")
public class UserEntity {
@Id
@GeneratedValue(strategy=GenerationType.AUTO) 
public Integer id;
	@Column(name = "name", nullable = false)
	public String name;
	@Column(name = "course", nullable = false)
	public String course;
	@Column(name = "age", nullable = false)
	public int age;
	@Column (name="userid",nullable =false)
	public int userid;
	
	
	 public UserEntity() {
		  
	    }




	public UserEntity(Integer id, String name, String course, int age) {
		super();
		this.id = id;
		this.name = name;
		this.course = course;
		this.age = age;
	}




	@Override
	public String toString() {
		return "id"+id + ", name=" + name + ", course=" + course + ", age=" + age;
	}




	public Integer getId() {
		return id;
	}




	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserid() {
		return userid;
	}




	public void setUserid(int userid) {
		this.userid = userid;
	}



	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getCourse() {
		return course;
	}




	public void setCourse(String course) {
		this.course = course;
	}




	public int getAge() {
		return age;
	}




	public void setAge(int age) {
		this.age = age;
	}

	 
	 

}
