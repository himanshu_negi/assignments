package com.Assignment.FirstAssignment.Repo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.Assignment.FirstAssignment.Enitity.UserEntity;

@Repository
public interface UserRepository  extends JpaRepository<UserEntity,Integer>{
List<UserEntity>findByUserid(int userid);
}
