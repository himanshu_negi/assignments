package com.example.JsonDataTypeTry;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface DataRepository extends JpaRepository<TestTable, Integer> {

}
