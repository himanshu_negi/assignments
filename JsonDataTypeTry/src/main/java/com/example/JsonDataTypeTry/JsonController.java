package com.example.JsonDataTypeTry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JsonController {
@Autowired
	private DataRepository dataRepository;
	
	@GetMapping	("/home")
	 public List<TestTable> getData()
	 {
		List<TestTable> list = (List<TestTable>) dataRepository.findAll();
		return  list; }
	
	@PostMapping("/addData")
	public TestTable setata( @RequestBody TestTable testTable  ){
		return dataRepository.save(testTable);
		
	}

}



