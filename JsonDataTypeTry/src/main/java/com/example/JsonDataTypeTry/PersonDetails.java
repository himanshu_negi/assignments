package com.example.JsonDataTypeTry;

import java.io.Serializable;

public class PersonDetails implements Serializable {

private static final long serialVersionUID = 1L;
private String name;
private int age;
private String address;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}
public PersonDetails(String name, int age, String address) {
	super();
	this.name = name;
	this.age = age;
	this.address = address;
}
public PersonDetails()
{
	}
@Override
public String toString() {
	return "PersonalDetails [name=" + name + ", age=" + age + ", address=" + address + "]";
}
 
	
	
}
