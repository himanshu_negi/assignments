package com.example.JsonDataTypeTry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
@Entity
@Table(name="Test")
@org.hibernate.annotations.TypeDef(name = "JsonType", typeClass = JsonType.class)
public class TestTable {
 @Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;

@Column(name = "info")
@Type(type = "JsonType")
private PersonDetails info;

public Integer getId() {
	return id;
}

public void setId(Integer id) {
	this.id = id;
}

public PersonDetails getInfo() {
	return info;
}

public void setInfo(PersonDetails info) {
	this.info = info;
}

@Override
public String toString() {
	return "TestTable [id=" + id + ", info=" + info + "]";
}
 public TestTable()
 {}


}
