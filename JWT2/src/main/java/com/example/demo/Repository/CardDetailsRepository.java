package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.CardDetails;

@Repository
public interface CardDetailsRepository extends JpaRepository<CardDetails, Integer> {

}
