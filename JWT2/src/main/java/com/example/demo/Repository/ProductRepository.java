package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.ProductDetails;


@Repository
public interface ProductRepository extends JpaRepository<ProductDetails, Integer> {
	ProductDetails findById(int id);



	



	List<ProductDetails> findByCategoryid(int catid);
	
}
