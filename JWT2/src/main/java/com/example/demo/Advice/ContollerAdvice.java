package com.example.demo.Advice;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.demo.Exception.EmptyInputException;
import com.example.demo.Exception.ExceptionResponse;
import com.example.demo.Exception.NoDataFoundException;




@ControllerAdvice
public class ContollerAdvice  extends ResponseEntityExceptionHandler {


@ExceptionHandler(EmptyInputException.class)
public ResponseEntity<String> emptyinputhandler(EmptyInputException emptyInputException)

{
	return new ResponseEntity<String>("empty input is not allowed for this field",HttpStatus.BAD_REQUEST);
	
}


@ExceptionHandler(NoDataFoundException.class)
public ResponseEntity<String> Nodatainput(NoDataFoundException noDataFoundException)
{
	return new ResponseEntity<String>("No Data Found", HttpStatus.NOT_FOUND);
}

@ExceptionHandler(NullPointerException.class)
public ResponseEntity<String> getnullmethod(NullPointerException nullObjectException)
{
	return new  ResponseEntity<String>("not found...!", HttpStatus.NOT_FOUND);
	}
@Override
protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
		HttpHeaders headers, HttpStatus status, WebRequest request) {
	

	List list = ex.getBindingResult().getAllErrors().stream()
            .map(fieldError -> fieldError.getDefaultMessage())
            .collect(Collectors.toList());
	ExceptionResponse res= new ExceptionResponse();
	res.setMessage(list.toString());
	res.setErrorcode(HttpStatus.BAD_REQUEST);
	ResponseEntity<Object> entity=new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
	
	return entity;
}


@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
	return new ResponseEntity<Object>("change method request type", HttpStatus.NOT_FOUND);
	}
}
