package com.example.demo.Exception;

import org.springframework.http.HttpStatus;



public class ExceptionResponse {
private String  message;
private HttpStatus errorcode;
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public HttpStatus getErrorcode() {
	return errorcode;
}
public void setErrorcode(HttpStatus errorcode) {
	this.errorcode = errorcode;
}



}
