package com.example.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.Entity.CardDetails;
//import io.jsonwebtoken.ExpiredJwtException;
import com.example.demo.Entity.Cart;
import com.example.demo.Entity.Category;
import com.example.demo.Entity.MyUser;
import com.example.demo.Entity.Orders;
import com.example.demo.Entity.Payment;
import com.example.demo.Entity.ProductDetails;
import com.example.demo.Exception.EmptyInputException;
import com.example.demo.Repository.CardDetailsRepository;
import com.example.demo.Repository.CartRepository;
import com.example.demo.Repository.CategoryRepository;
import com.example.demo.Repository.OrderTableRepository;
import com.example.demo.Repository.PaymentRepository;
import com.example.demo.Repository.ProductRepository;
import com.example.demo.Repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@RestController
public class JWTController {

//	@Autowired
//	private AuthenticationManager authenticationManager;
	@Autowired
	private Payment payment;
	@Autowired
	private CardDetails carddetails;
	@Autowired
	private OrderTableRepository orderTableRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private PaymentRepository paymentRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	MyUser myUser;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private CustomUserDetailService customUserDetailService;
	@Autowired
	CartRepository cartRepository;
	@Autowired
	private JWTUtil jwtUtil;
	@Autowired
	private CardDetailsRepository cardDetailsRepository;
	@Autowired
	private MyUser temp;

	@RequestMapping(value = "/token", method = RequestMethod.POST)
	public ResponseEntity<?> generateToken(@RequestBody MyUser myuser) throws Exception {

//		try {
//			this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(myUser.getEmail(), myUser.getPassword()));
//		}
//		catch(UsernameNotFoundException e) {
//			e.printStackTrace();
//			throw new Exception("Bad Credentials");
//		}
//		catch(BadCredentialsException b) {
//			throw new Exception("Bad Credentials");
//		}
//		catch(ExpiredJwtException e) {
//			return ResponseEntity.ok(new JWTResponse("Token expired!"));
//		}
//		catch(NullPointerException e) {
//			return ResponseEntity.ok(new JWTResponse("Empty"));
//		}

		// for generating token
//		UserDetails userDetails = this.customUserDetailService.loadUserByUsername(jwtRequest.getUsername());
//		
//		String token =  this.jwtUtil.generateToken(userDetails);
//		System.out.println(token);
//		System.out.println("-------> "+this.jwtUtil.extractExpiration(token));

		// {"token":"value"}
		// this will convert it into json

		temp = this.customUserDetailService.findUser(myuser.getEmail(), myuser.getPassword());
		if (temp != null) {
			log.info("succesfully logged in..!!{}",temp.getEmail());
		} else {
			 log.error("log-in failed{}",temp.getEmail());
		}
		
		String token = this.jwtUtil.generateToken(temp);
		return ResponseEntity.ok(new JWTResponse(token));
//		return role;

	}

	@PostMapping("/mask")
	public String getUserSpecificorder(@RequestBody CardDetails cardDetails) {
		carddetails.setCardidmask(cardDetails.getCardid());
		System.out.println(carddetails);
		cardDetailsRepository.save(cardDetails);
		return carddetails.toString();
	}

	@GetMapping("/mask")
	public List<CardDetails> getdatamask() {
		List<CardDetails> res = new ArrayList<CardDetails>();
		
		List<CardDetails> c = cardDetailsRepository.findAll();
		for (CardDetails card : c) {
			card.setCardidmask(card.getCardid());
			res.add(card);

		}if(res.isEmpty())
		log.info("table is empty");
		
		log.info("data found..{}",res);
		return res;
	}

	@PostMapping("/serachbycategories")
	public Map<Integer, Integer> getproductbycategory(@RequestBody Category category) {
		List<Integer> pidlist = new ArrayList<Integer>();
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		Category catid = categoryRepository.findByCatname(category.getCatname());

		List<ProductDetails> list = productRepository.findAll();
		if(list.isEmpty())
			log.info("table is empty");
		for (ProductDetails p : list) {
			if (p.getCategoryid() == catid.getCatid()) {
				pidlist.add(p.getId());
			}
		}

		for (int i : pidlist) {
			int total = 0;
			List<Cart> cart = cartRepository.findAll();
			for (Cart c : cart) {
				if (c.getPid() == i && c.getIsincart().equals("false")) {
					total = total + c.getPquantity();

				}
			}
			map.put(i, total);
		}
		log.info("data found..");
		return map;
	}

	@PostMapping("/userspecificorder")
	public List<Orders> getUserSpecificorder(@RequestBody Orders order) {
		List<Orders> print = new ArrayList<Orders>();
		
		List<Orders> p = orderTableRepository.findAll();
		for (Orders o : p) {
			if (o.getCustomerid().equals(order.getCustomerid())) {
				print.add(o);
			}
		}
		if(print.isEmpty())
		{log.info("table is empty");
		}
		log.info("data found..");
		return print;
	}

	@PostMapping("/supplierspecificproduct")
	public List<ProductDetails> getSpecificproduct(@RequestBody ProductDetails productDetails) {
		List<ProductDetails> print = new ArrayList<>();
		List<ProductDetails> p = productRepository.findAll();
		if(p.isEmpty())
			log.info("table is empty");
		for (ProductDetails o : p) {
			if (o.getSupplierid().equalsIgnoreCase(productDetails.getSupplierid())
					&& (o.getIsactive().equals("true"))) {
				print.add(o);
			}

		}
		if (print.size() == 0)
			return null;
		log.info("data found..");
		return print;
	}

	@GetMapping("/orderview")
	public List<Orders> getcartdata() {
		List<Orders> print = new ArrayList<Orders>();
		
		List<Orders> p = orderTableRepository.findAll();
		for (Orders o : p) {
			if (o.getCustomerid().equals(temp.getEmail())) {
				print.add(o);
			}
		}
		if(print.isEmpty())
			log.info("table is empty");
		log.info("data found..");
		return print;
	}

	@PostMapping("/order")
	public String Makepayment(@RequestBody Orders orderTable) {
		int total = 0;
		int quantity = 0;
		List<String> pnamelist = new ArrayList<String>();

		List<Cart> list = cartRepository.findAll();
		if(list.isEmpty())
			log.info("cart is empty");
		for (Cart cart : list) {
			if (cart.getUserid().equals(temp.getEmail()) && cart.getIsincart().equals("true")
					&& cart.getIsactive().equals("true")) {

				total = cart.getTotal() + total;
				pnamelist.add(cart.getPname());
				ProductDetails pd = productRepository.findById(cart.getPid());
				if (!pd.getIsactive().equals("true"))
					return " product not avialble " + pd.getPname() + " Pid " + pd.getId();
				if (cart.getPquantity() <= pd.getQuantity()) {
					quantity = pd.getQuantity() - cart.getPquantity();
					pd.setQuantity(quantity);
					this.productRepository.save(pd);

				} else {log.warn("decrese no of quantity of "+pd.getPname());
					return "decrese no of quantity of " + pd.getPname() + " Pid " + pd.getId();
				}
				cart.setIsincart("false");
				cartRepository.save(cart);
			}

		}
		if (total == 0)
		{log.info("no item in cart");
			return "no item in cart";
	}
		orderTable.setDeliveryaddress(orderTable.getDeliveryaddress());
		orderTable.setAmount(total);
		orderTable.setCustomerid(temp.getEmail());
		orderTable.setProductname(pnamelist);
		LocalDate today = LocalDate.now();

		orderTable.setDateÏ(today.toString());

		orderTableRepository.save(orderTable);
		payment.setAmount(total);
		payment.setCustomerid(temp.getEmail());
		if (orderTable.getPaymentmode().equals("online")) {
			payment.setPaymentstatus("completed");
		} else {
			payment.setPaymentstatus("pending");
		}
		payment.setOrderid(orderTable.getOrderid());
		paymentRepository.save(payment);
		log.info("order placed succesfully");
		return orderTable.toString();

	}

	@PostMapping("/categoryadd")
	public String addcategory(@RequestBody Category category) {
		if ((categoryRepository.findByCatid(category.getCatid()) != null)
				|| (categoryRepository.findByCatname(category.getCatname()) != null))
			{log.info("category already defined");
			return "cann't save category,category already defined...!!";
	
			}if (category.getCatname().length() == 0)
			{log.error("empty input is not allowed");
				throw new EmptyInputException("101", "Empty Input is not allwed");
	}this.categoryRepository.save(category);
	log.info("catgeory added succesfuly..");
		return "category added...!!";

	}

	@PutMapping("/categoryadd/{catid}")
	public String updatecategory(@PathVariable int catid, @RequestBody Category category) {
		Category cat = categoryRepository.findByCatid(catid);
		if (cat == null)
			log.error("category id does not found...!!");
		cat.setCatid(category.getCatid());
		cat.setCatname(category.getCatname());
		this.categoryRepository.save(cat);
		log.info("catgeory updated succesfuly..");
		return "category updated...!!";

	}

	@GetMapping("/categoryview")
	public List<Category> getallCateview() {
		List<Category> cat = categoryRepository.findAll();
if(cat.isEmpty())
	log.info("category table is empty");
log.info("data found..");
		return cat;

	}

	@GetMapping("/categoryview/{categoryname}")
	public List<ProductDetails> getCatview(@PathVariable String categoryname) {
		Category cat = categoryRepository.findByCatname(categoryname);
		if (cat == null)
			log.error("no categories available");

		List<ProductDetails> list = productRepository.findByCategoryid(cat.getCatid());
       log.info("categories shown successfully");
		return list;

	}

	@GetMapping("/cart/view")
	public List<Cart> cartProductview() {

		List<Cart> list = cartRepository.findAll();
		if(list.isEmpty())
			log.info("table is empty");
		List<Cart> res = new ArrayList<Cart>();
		for (Cart c : list) {
			if (c.getUserid().equals(temp.getEmail()) && (c.getIsactive().equals("true"))
					&& (c.getIsincart().equalsIgnoreCase("true")))
				res.add(c);
		}if(res.isEmpty())
			log.info("table is empty");	
		log.info("data found..");
		return res;
	}

	@PostMapping("/cart/add")
	public String addCart(@RequestBody Cart c) {
		ProductDetails m = productRepository.findById(c.getPid());
		if (m == null)
			log.error("product not available");
		if (m.getQuantity() == 0)
		{log.warn("out of stock...!!");
			return "out of stock";
	}		List<Cart> list = cartRepository.findAll();
		for (Cart cart : list) {
			if (cart.getUserid().equals(temp.getEmail()) && (cart.getIsactive().equals("true"))
					&& (cart.getPid() == c.getPid())) {
				
				log.warn("item already in cart..!");
				return "item alredy in cart";

			}
		}
		if (c.getPquantity() >= m.getQuantity())
		{ log.warn("decrease no of quantity");
			return "decrease no of quantity";
		
		}if (m.getIsactive().equals("true")) {
			c.setUserid(temp.getEmail());
			c.setPname(m.getPname());
			int total = c.getPquantity() * m.getPrice();
			c.setTotal(total);
			this.cartRepository.save(c);
        log.info("data added in cart succesfulyy..!!");
			return c.toString();
		}
		log.info("product not available");
		return "product not available";
	}

	@DeleteMapping("cart/delete/{pid}")
	public String cartdeletedata(@PathVariable int pid) {
		List<Cart> list = (List<Cart>) cartRepository.findAll();
if(list.isEmpty())
	log.warn("table is empty");
		for (Cart c : list) {
			if (c.getUserid().equals(temp.getEmail()) && (c.getIsactive().equals("true")) && (c.getPid() == pid)) {
				System.out.println(c);
				c.setIsactive("false");

				cartRepository.save(c);
				log.info("data deleted successfully");
				return "data deleted successfully";
			}

		}
		log.warn("data not found");
		return "data not found";
	}

	@GetMapping("/cart")
	public List<Cart> show() {
		List<Cart> c = cartRepository.findAll();
if(c.isEmpty())
	log.info("cart is empty");
		return c;

	}

	@PutMapping("cart/update/{pid}")
	public String updatecartdata(@PathVariable Integer pid, @RequestBody Cart c) {
		ProductDetails m = productRepository.findById(pid);
	if (m == null)
log.error("product does not exist");
		List<Cart> list = cartRepository.findAll();

		for (Cart cart : list) {
			if (cart.getUserid().equals(temp.getEmail()) && (cart.getIsactive().equals("true"))
					&& (cart.getPid() == pid)) {
				if (m.getQuantity() == 0) {
					log.warn("out of stock");
					return "out of stock";
				}
				if (c.getPquantity() >= m.getQuantity())
				{log.warn("decrese quanity no");
					return "decrease quantity no";
			}
				cart.setPquantity(c.getPquantity());
				int ptotal = m.getPrice() * c.getPquantity();
				cart.setTotal(ptotal);
				cartRepository.save(cart);
				log.info("data updated succesfully");
				return "data updated successfully";
			}

		}
		log.info("data not found...");
		return "data not found";
	}

	@RequestMapping("/test")
	public String test() {
		return "testing purpose";
	}

	@GetMapping("/products")
	public List<ProductDetails> allProduct() {

		List<ProductDetails> list = productRepository.findAll();
		if(list.isEmpty())
			log.info("table is empty");
		List<ProductDetails> res = new ArrayList<ProductDetails>();
		for (ProductDetails p : list) {
			if (!p.getIsactive().equals("false"))
				res.add(p);
		}
		if(res.isEmpty())
			log.info("table is empty");	
		log.info("data found");
		return res;
	}

	@GetMapping("/products/view")
	public List<ProductDetails> allProductview() {

		List<ProductDetails> list = productRepository.findAll();
	if(list.isEmpty())
	{
		log.info("product table is empty");
	}
		List<ProductDetails> res = new ArrayList<ProductDetails>();
		for (ProductDetails p : list) {
			if (p.getSupplierid().equals(temp.getEmail()) && (p.getIsactive().equals("true")))
				res.add(p);
		}
		if(res.isEmpty())
			log.info("table is empty");
		log.info("data found...");
		return res;
	}

	@PostMapping("products/add")
	public String addProducts(@RequestBody ProductDetails productDetails) {
		if (productRepository.findById(productDetails.getId()) != null)
		{log.warn("data already added..!!");
			return "data already added";
	}

		productDetails.setSupplierid(temp.getEmail());
		this.productRepository.save(productDetails);
		log.info("data added succeessfully.. by"+temp.getEmail());
		return "data added succesfully....";
	}

	@PutMapping("products/update/{id}")
	public String setdata(@PathVariable Integer id, @RequestBody ProductDetails productDetails) {
		ProductDetails p = productRepository.findById(id);
		if (p == null)
			log.error("product not found...!!");
		if (p.getSupplierid().equals(temp.getEmail()) && (p.getIsactive().equals("true"))) {
			p.setPname(productDetails.getPname());
			p.setPrice(productDetails.getPrice());
			p.setQuantity(productDetails.getQuantity());
			p.setDiscount(productDetails.getDiscount());

			productRepository.save(p);
			log.info("data updated successfully by"+p.getSupplierid());
			return " data updated succesfully";
		} else {log.warn("Supplier id does not match. or product is Deleted from db..!!");
			return "data cannt be updated ,supplierid mismatch or product is Deleted from db";
		}
	}

	@DeleteMapping("products/delete/{id}")
	public String deletedata(@PathVariable int id) {
		ProductDetails m = productRepository.findById(id);
		if (m == null)
         log.error("product not found...!!");
		if (m.getSupplierid().equals(temp.getEmail()) && (m.getIsactive().equals("true"))) {
			m.setIsactive("false");
			productRepository.save(m);
			log.info("data deleted successfully ...!");
			return "data deleted succsesfully";
		} else {
			log.warn("Supplier id does not match or Data is deleted from DB..!!");
			return "data cannt be deleted ,supplierid mismatch or data not exist";
		}

	}

	@RequestMapping("/userlist")
	public List<MyUser> allData() {
		return this.userRepository.findAll();
	}

	@PostMapping("/signup")
	public String add(@Valid @RequestBody MyUser myusr) {
		if (myusr.getEmail().isEmpty() || myusr.getPassword().isEmpty()) {
			log.error("empty values are not allowed");
			throw new EmptyInputException();

		}
		this.userRepository.save(myusr);
		return "successfully signup";
	}

}
