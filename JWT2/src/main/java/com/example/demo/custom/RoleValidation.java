package com.example.demo.custom;
import java.util.Arrays;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class RoleValidation  implements ConstraintValidator<RoleValidator, String>
{

	

	@Override
	public boolean isValid(String roles, ConstraintValidatorContext context) {
		List list = Arrays.asList(new String[]{"user","admin","supplier"});
        return list.contains(roles);

	}

	@Override
	public void initialize(RoleValidator constraintAnnotation) {
		// TODO Auto-generated method stub
		
	}
}


