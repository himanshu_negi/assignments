package com.example.demo;

import java.lang.reflect.Field;

import com.example.demo.custom.MaskChar;


public class CardUtil {

	private CardUtil() {
    }

public static String maskStringField(Class<?> clss, final String fieldName, final String currValue) {
    for (Field field : clss.getDeclaredFields()) {
        String fName = field.getName();

       if (field.isAnnotationPresent(MaskChar.class)) {
                MaskChar mask = field.getAnnotation(MaskChar.class);
                if (mask.value() != ' ') {
                    String part1 = currValue.substring(0, currValue.length() - 4).replaceAll("[0-9]",
                            String.valueOf(mask.value()));

                    String part2 = currValue.substring(currValue.length() - 4);

                    return part1 + part2;
                }
            }
        }
    

    return currValue;
}

}
