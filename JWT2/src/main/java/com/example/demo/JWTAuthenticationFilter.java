package com.example.demo;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JWTAuthenticationFilter extends OncePerRequestFilter{

	@Autowired
	private JWTUtil jwtUtil;
	
	@Autowired
	private CustomUserDetailService customUserdetailService;
 	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException, ExpiredJwtException , NullPointerException{
		//get jwt -> bearer -> validate
		
		String requestTokenHeader = request.getHeader("Authorization");
		String username = null;
		String jwtToken = null;
		//List<GrantedAuthority>  la = new ArrayList<>();
		//checking null and format
		if(requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			jwtToken = requestTokenHeader.substring(7);
			
			try {
				if(this.jwtUtil.isTokenExpired(jwtToken)) {	
					System.out.println("---->Token expired!");
					throw new ExpiredJwtException(null, null, jwtToken);
				}
				username = this.jwtUtil.extractUsername(jwtToken);
				
				
			}catch(Exception e) {
				
			}
			
			UserDetails userdetails = this.customUserdetailService.loadUserByUsername(username);

			// userdetails = this.customUserdetailService.loadUserByUsername(username);
			
			
			//MyUser userdetails = this.customUserdetailService.findUser(username, password);
			
			
			if(userdetails == null) {
				throw new NullPointerException("Userdetails are null");
			}
			
			if(username!=null && SecurityContextHolder.getContext().getAuthentication() == null ) {
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =  new UsernamePasswordAuthenticationToken(userdetails, null,userdetails.getAuthorities());
				usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}
			else {
				System.out.println("Token is not validated");
			}
		}	
		filterChain.doFilter(request, response);
	}	
}
