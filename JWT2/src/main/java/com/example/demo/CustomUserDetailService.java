package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.MyUser;

import com.example.demo.Repository.UserRepository;

import ch.qos.logback.classic.Logger;

@Service
public class CustomUserDetailService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@SuppressWarnings("unused")
	public MyUser findUser(String username, String password) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		// User user= null;
		MyUser temp = this.userRepository.findByEmail(username);
		
		System.out.println(temp.getPassword() + "---" + password);
		if (temp.getPassword().equals(password)) {
			return temp;
		}
		return null;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		// User user= null;
		MyUser temp = this.userRepository.findByEmail(username);
		if (temp != null) {
			List<GrantedAuthority> authority = new ArrayList<>();
			authority.add(new SimpleGrantedAuthority(temp.getProfile()));
			System.out.println(authority);
			return new User(temp.getEmail(), temp.getPassword(), authority);
		} else
			throw new UsernameNotFoundException("User not found");
	}
}
