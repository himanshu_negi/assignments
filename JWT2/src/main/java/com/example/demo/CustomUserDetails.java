//package com.example.demo;
//
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//
//public class CustomUserDetails implements UserDetails {
//
//	private JWTRequest jwtUser;
//	
//	
//	public CustomUserDetails(JWTRequest jwtUser) {
//		super();
//		this.jwtUser = jwtUser;
//	}
//
//	@Override
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(jwtUser.getProfile());
//		List<SimpleGrantedAuthority> s= new ArrayList<>();
//		s.add(simpleGrantedAuthority);
//		return s;
//	}
//
//	@Override
//	public String getPassword() {
//		// TODO Auto-generated method stub
//		return jwtUser.getPassword();
//	}
//
//	@Override
//	public String getUsername() {
//		// TODO Auto-generated method stub
//		return jwtUser.getUsername();
//	}
//
//	@Override
//	public boolean isAccountNonExpired() {
//		// TODO Auto-generated method stub
//		return true;
//	}
//
//	@Override
//	public boolean isAccountNonLocked() {
//		// TODO Auto-generated method stub
//		return true;
//	}
//
//	@Override
//	public boolean isCredentialsNonExpired() {
//		// TODO Auto-generated method stub
//		return true;
//	}
//
//	@Override
//	public boolean isEnabled() {
//		// TODO Auto-generated method stub
//		return true;
//	}
//
//}
