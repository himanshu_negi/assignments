package com.example.demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
//@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {

//	@Autowired
//	private CustomUserDetailService customUserDetailService;

	@Autowired
	private JWTAuthenticationFilter jwtFilter;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().cors().disable().authorizeRequests()
		.antMatchers("/signup").permitAll()
				.antMatchers("/token").permitAll()
				.antMatchers("/cart").hasAuthority("admin")
				.antMatchers("/cart/add").hasAuthority("user")
				.antMatchers("/order").hasAuthority("user")
				.antMatchers("/orderview").hasAuthority("user")
				.antMatchers("/cart/delete/**").hasAuthority("user")
				.antMatchers("/cart/update/**").hasAuthority("user")
				.antMatchers("/cart/view/**").hasAuthority("user")
				.antMatchers("/products/delete/**").hasAuthority("supplier")
				.antMatchers("/products/update/**").hasAuthority("supplier")
				.antMatchers("/products/view").hasAuthority("supplier")
				.antMatchers("/products/add").hasAuthority("supplier")
				.antMatchers("/products").hasAnyAuthority("user", "admin")
				.antMatchers("/userlist").hasAuthority("admin")
				.antMatchers("/categoryadd/**").hasAuthority("admin")
				.antMatchers("/categoryview/*").hasAnyAuthority("user", "admin", "supplier")
				.antMatchers("/userspecificorder").hasAuthority("admin")
				.antMatchers("/supplierspecificproduct").hasAuthority("admin")
				.antMatchers("/serachbycategories").hasAuthority("admin")
				.antMatchers("/admin").hasAuthority("admin")
				 .antMatchers("/cart/**").hasAuthority("user")
				.anyRequest().authenticated().and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);

	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
		// auth.userDetailsService(customUserDetailService);

	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/mask");
		web.ignoring().antMatchers("/categoryadd");
		}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());

		return authProvider;
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

//	@Bean
//	public AuthenticationManager authenticationManagerBean() throws Exception {
//		return super.authenticationManager();
//	}

}
