package com.example.demo.Entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class Orders {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer orderid;
	@Column(name = "customerid")
	private String customerid;

	@Column(name = "amount")
	private int amount;

	@ElementCollection
	@Column(name = "productname")
	private List<String> productname;
	@Column(name = "paymentmode")
	private String paymentmode;
	@Column(name = "deliveryaddress")
	private String deliveryaddress;
	@Column(name = "pincode")
	private int pincode;
	@Column(name = "date")
	private String dateÏ;

	public Integer getOrderid() {
		return orderid;
	}

	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}

	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public List<String> getProductname() {
		return productname;
	}

	public void setProductname(List<String> productname) {
		this.productname = productname;
	}

	public String getPaymentmode() {
		return paymentmode;
	}

	public void setPaymentmode(String paymentmode) {
		this.paymentmode = paymentmode;
	}

	public String getDeliveryaddress() {
		return deliveryaddress;
	}

	public void setDeliveryaddress(String deliveryaddress) {
		this.deliveryaddress = deliveryaddress;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public Orders(Integer orderid, String customerid, int amount, List<String> productname, String paymentmode,
			String deliveryaddress, int pincode, String dateÏ) {
		super();
		this.orderid = orderid;
		this.customerid = customerid;
		this.amount = amount;
		this.productname = productname;
		this.paymentmode = paymentmode;
		this.deliveryaddress = deliveryaddress;
		this.pincode = pincode;
		this.dateÏ = dateÏ;
	}

	@Override
	public String toString() {
		return "Orders [orderid=" + orderid + ", customerid=" + customerid + ", amount=" + amount + ", productname="
				+ productname + ", paymentmode=" + paymentmode + ", deliveryaddress=" + deliveryaddress + ", pincode="
				+ pincode + ", dateÏ=" + dateÏ + "]";
	}

	public Orders() {
		super();
	}

	public String getDateÏ() {
		return dateÏ;
	}

	public void setDateÏ(String today) {
		this.dateÏ = today;
	}

}
