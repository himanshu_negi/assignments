package com.example.demo.Entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cart")
public class Cart {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@Column(name = "userid")
	private String userid;
	@Column(name = "pname")
	private String pname;
	@Column(name = "pquantity")
	private int pquantity;
	@Column(name = "total")
	private int total;
	@Column(name = "pid")
	private int pid;
	
	@Column(name = "isactive")
	private String isactive = "true";
	@Column(name = "isincart")
	private String isincart="true";
	

	public String getIsincart() {
		return isincart;
	}

	public void setIsincart(String isincart) {
		this.isincart = isincart;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public int getPquantity() {
		return pquantity;
	}

	public void setPquantity(int pquantity) {
		this.pquantity = pquantity;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	

	public Cart(Integer id, String userid, String pname, int pquantity, int total, int pid, String isactive,String isincart) {
		super();
		this.id = id;
		this.userid = userid;
		this.pname = pname;
		this.pquantity = pquantity;
		this.total = total;
		this.isactive = isactive;
		this.pid = pid;
		this.isincart=isincart;
	}


	@Override
	public String toString() {
		return "Cart [id=" + id + ", userid=" + userid + ", pname=" + pname + ", pquantity=" + pquantity + ", total="
				+ total + ", pid=" + pid + ", isactive=" + isactive + ", isincart=" + isincart + "]";
	}

	public Cart() {

	}

}
