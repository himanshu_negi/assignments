package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "productdetails")
public class ProductDetails {
	@Id
	// @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "pname")
	private String pname;
	@Column(name = "price")
	private int price;
	@Column(name = "quantity")
	private int quantity;
	@Column(name = "supplierid")
	private String supplierid;
	@Column(name = "discount")
	private int discount;
	@Column(name = "categoryid")
	private int categoryid;

	public int getCategoryid() {
		return categoryid;
	}

	public void setCategory(int categoryid) {
		this.categoryid = categoryid;
	}

	@Column(name = "isactive")
	private String isactive = "true";

	public String getSupplierid() {
		return supplierid;
	}

	public void setSupplierid(String supplierid) {
		this.supplierid = supplierid;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public ProductDetails(Integer id, String pname, int price, int quantity, int discount, String isactive,
			int categoryid, String supplierid) {
		super();
		this.id = id;
		this.pname = pname;
		this.price = price;
		this.quantity = quantity;
		this.isactive = isactive;
		this.discount = discount;
		this.supplierid = supplierid;
		this.categoryid = categoryid;
	}

	@Override
	public String toString() {
		return "ProductDetails [pid=" + id + ", pname=" + pname + ", price=" + price + ", quantity=" + quantity
				+ ", discount=" + discount + "]";
	}

	public ProductDetails() {
	}
}
