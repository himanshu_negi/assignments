package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="category")
public class Category {
@Id
@Column(name="catid")
private Integer catid;
@Column(name="catname")
private String catname;
public int getCatid() {
	return catid;
}
public void setCatid(int catid) {
	this.catid = catid;
}
public String getCatname() {
	return catname;
}
public void setCatname(String catname) {
	this.catname = catname;
}
@Override
public String toString() {
	return "Category [catid=" + catid + ", catname=" + catname + "]";
}
public Category(int catid, String catname) {
	super();
	this.catid = catid;
	this.catname = catname;
}
public Category() {
	
}


}
