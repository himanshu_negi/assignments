package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.example.demo.CardUtil;
import com.example.demo.custom.MaskChar;

@Component
@Entity
@Table(name="carddetail")
public class CardDetails {
@Id
@GeneratedValue
(strategy = GenerationType.AUTO)
private int id;
@MaskChar
@Column(name="cardid")
private String cardid;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getCardid() {
	return cardid;
} 

public void setCardid(String cardid)
{this.cardid=cardid;
	}
public String setCardidmask(String cardid) {
   return this.cardid = CardUtil.maskStringField(CardDetails.class, "identifier",cardid);
}

public CardDetails(int id, String cardid) {
	super();
	this.id = id;
	this.cardid = cardid;
}
@Override
public String toString() {
	return "CardDetails [id=" + id + ", cardid=" + cardid + "]";
}
public CardDetails() {
	super();
}



}
