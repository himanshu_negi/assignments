package com.example.demo.Entity;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "paymentorder")
public class Payment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer paymentid;
	@Column(name = "amount")
	private int amount;
	@Column(name = "customerid")
	private String customerid;

	@Column(name = "paymentstatus")
	private String paymentstatus;
	@Column(name = "orderid")
	private int orderid;

	public Integer getPaymentid() {
		return paymentid;
	}

	public void setPaymentid(Integer paymentid) {
		this.paymentid = paymentid;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public String getPaymentstatus() {
		return paymentstatus;
	}

	public void setPaymentstatus(String paymentstatus) {
		this.paymentstatus = paymentstatus;
	}

	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}

	public Payment(Integer paymentid, int amount, String customerid, String paymentstatus, int orderid) {
		super();
		this.paymentid = paymentid;
		this.amount = amount;
		this.customerid = customerid;
		this.paymentstatus = paymentstatus;
		this.orderid = orderid;
	}

	@Override
	public String toString() {
		return "Payment [paymentid=" + paymentid + ", amount=" + amount + ", customerid=" + customerid
				+ ", paymentstatus=" + paymentstatus + ", orderid=" + orderid + "]";
	}

	public Payment() {
		super();
	}

}
