package com.springboot.welcome.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.springboot.welcome.entity.Hello;
import com.springboot.welcome.services.HelloService;

@RestController
public class WelcomeController {
	
	@Autowired
	private HelloService helloService;
	
	
	@GetMapping("/home")
	public String home() {
		return "Welcom to my first SpringBoot Project";
	}
	
	@GetMapping("/hellos")
	public List<Hello> getHellos(){
		//for loose coupling
		return this.helloService.getHellos();
	}
	
	@GetMapping("/hellos/{id}")
	public Hello getHello(@PathVariable String id) {
		return this.helloService.getHello(Integer.parseInt(id));
	}
	
	@PostMapping("/hellos")
	public Hello addHello(@RequestBody Hello hello) {
		return this.helloService.addHello(hello);
		
	}
	
	@PutMapping("/hellos")
	public Hello updateHello(@RequestBody Hello hello) {
		 return this.helloService.updateHello(hello);
		
	}
	
//	@DeleteMapping("/hellos")
//	public void deleteHello(@RequestBody Hello hello) {
//		 this.helloService.deleteHello(hello);
//		
//	}
	
	@DeleteMapping("/hellos/{id}")
	public void deleteHello(@PathVariable String id) {
		 this.helloService.deleteHello(Integer.parseInt(id));
		
	}
	
	
	
}
