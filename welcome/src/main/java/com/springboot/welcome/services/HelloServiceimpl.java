package com.springboot.welcome.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.springboot.welcome.entity.Hello;

@Service
public class HelloServiceimpl implements HelloService {
	
	List<Hello> list;
	
	public HelloServiceimpl() {
		list = new ArrayList<>();
		list.add(new Hello(1,"Hello"));
		list.add(new Hello(2,"Hola"));
		list.add(new Hello(3,"Bello"));
	}
	
	@Override
	public List<Hello> getHellos() {
		// TODO Auto-generated method stub
		
		return list;
	}

	@Override
	public Hello getHello(int id) {
		Hello temp = null;
		for(Hello hello : list) {
			if(hello.getId()==id) {
				temp=hello;
				break;
			}
		}
		return temp;
	}

	@Override
	public Hello addHello(Hello hello) {
		list.add(hello);
		return hello;
	}
	
	@Override
	public Hello updateHello(Hello hello) {
		for(Hello temp : list) {
			if(temp.getId()==hello.getId()) {
				//Both these methods not working because we are not getting correct index in them
				//list.set(temp.getId(), hello);
				//temp=hello;
				int x=list.indexOf(temp);
				list.set(x,hello);
				break;
			}
		}
		return hello;
	}

	@Override
	public void deleteHello(int id) {
		for(Hello temp : list) {
		if(temp.getId()==id) {
			list.remove(temp);
			break;
		}
		
	}
}

//	@Override
//	public void deleteHello(Hello hello) {
//		
//		for(Hello temp : list) {
//			if(temp.getId()==hello.getId()) {
//				
//				list.remove(temp);
//				break;
//			}
//		}
//	}
	
	

}
