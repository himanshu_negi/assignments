package com.springboot.welcome.services;

import java.util.List;

import com.springboot.welcome.entity.Hello;

public interface HelloService {

	public List<Hello> getHellos();

	public Hello getHello(int id);
	public Hello addHello(Hello hello);
	public Hello updateHello(Hello hello);
	public void deleteHello(int id);
	//public void deleteHello(Hello hello);
}
