package AerospikeTry.AerospikeTry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AerospikeTryApplication {

	public static void main(String[] args) {
		SpringApplication.run(AerospikeTryApplication.class, args);
		
	}

}
