package com.example.gson;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.google.gson.Gson;

@SpringBootApplication
public class GsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(GsonApplication.class, args);
	
	
		Entity t = new Entity(1,"himanshu","delhi");
//List<Entity> list= new ArrayList<Entity>();
//list.add(new Entity(2,"harsh","gfd"));
//list.add(new Entity(3,"ansh","fdfd"));
//list.add(new Entity(4,"harshit","ddgfd"));

Gson gson = new Gson();
		String empJson = gson.toJson(t);
		System.out.println("object to json"+empJson);
//		String listjosn= gson.toJson(list);
//		System.out.println(listjosn);
		Entity empGenerated = gson.fromJson(empJson, Entity.class);
	 
	   
	        System.out.println("json to object"+ empGenerated);
	    }
		

}
