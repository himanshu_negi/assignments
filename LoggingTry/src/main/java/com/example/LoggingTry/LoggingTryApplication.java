package com.example.LoggingTry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoggingTryApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoggingTryApplication.class, args);
	}

}
