package com.example.LoggingTry;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.qos.logback.classic.Logger;

@RestController
public class LoggingController {

	
	private static Logger logger = (Logger) LoggerFactory.getLogger(Controller.class);
	
	@GetMapping("/home")
	public String home() {
		logger.error("error ocurred");
		logger.debug("debugg");
		logger.info("info");
		logger.warn("warn");
		logger.trace("trace");
		return "hello there";
	}

	
	
}
